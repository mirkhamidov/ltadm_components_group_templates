<?
$items=array(
	"name" => array(
		"desc"				=> "Название группы шаблонов",
		"type"				=> "text",
		"maxlength"			=> "255",
		"size"				=> "30",
		"select_on_edit"	=> true,
		"js_not_empty"		=> true,
		"js_error"			=> "Поле должно быть не пустым!",
	),
	"foldername" => array(
		"desc"			=> "Имя папки для шаблонов",
		"type"			=> "folder",
		"path"			=> "../templates/",
		"maxlength"		=> "50",
		"size"			=> "10",
		"js_validation"	=> array(
			"js_not_empty"	=> "Поле должно быть не пустым!",
			"js_match"		=> array (
				"pattern"	=> "^[A-Za-z0-9_\-.]+$",
				"flags"		=> "g",
				"error"		=> "Только латинские символы, цифры и '_','-'!",
			),
		),
	),
);
?>