<?
$config=array(
	"name"		=> "Группы шаблонов",
	"menu_icon"	=> "icon-bar-chart",		
	"status"	=> "system",		
	"windows"	=> array(			
		"create"	=> array("width" => 700,"height" => 200),
		"edit"		=> array("width" => 800,"height" => 300),
		"list"		=> array("width" => 600,"height" => 500),
	),
	"right"			=> array("admin","#GRANTED"),
	"main_table"	=> "lt_group_templates",
	"list"			=> array(
		"name"			=> array("isLink" => true),
		"foldername"	=> array("align" => "center"),
	),
	"select" => array(
		"max_perpage" => 20,
		"default_orders" => array(
			array("name" => "ASC")
		),
	 	"default" => array(
			"id_lt_group_templates" => array(
				"desc"			=> "Группа шаблонов",
				"type"			=> "select_from_table",
				"table"			=> "lt_group_templates",
				"key_field"		=> "id_lt_group_templates",
				"fields"		=> array("name"),
				"show_field"	=> "%1",
				"condition"		=> "",
				"order"			=> array ("name" => "ASC"),
				"use_empty"		=> true,
				"in_list"		=> true,
			),
		),
	),
	"downlink_tables" => array(
		"lt_templates" => array(
			"key_field" => "id_lt_group_templates",
			"name" => "name",
			"component" => "templates_in_group"
		),
	),
);

$actions=array(
	"create" => array(
		"before_code" => "",
	),
);

?>